FROM python:latest
WORKDIR /src
RUN pip install rich
COPY . /src
CMD ["python", "./src/Taskmanager"]
