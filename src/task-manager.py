import sqlite3
from rich import print
from rich.table import Table

conn = sqlite3.connect("tasks.db")
c = conn.cursor()

def create_table_tasks():
    with conn:
        c.execute("CREATE TABLE IF NOT EXISTS tasks (task text, time text, status integer, task_no integer)")


def create_table_completed_tasks():
    with conn:
        c.execute("CREATE TABLE IF NOT EXISTS completed_tasks (task text, time text, status integer, task_no integer)")


def task_list_ui():
    tasks = []
    for i in (c.execute("SELECT * FROM tasks")):
        tasks.append(i)
    
    table = Table()
    table.add_column("Task")
    table.add_column("Time to complete")
    table.add_column("Status")
    table.add_column("#")

    for i in tasks:
        if i[2] == 2:
            emoji = "✅"
        elif i[2] == 1:
            emoji = "WIP🛠"
        else:
            emoji = "❌"

        table.add_row((str(i[0])), (str(i[1])), emoji, str(i[3]))

    print(table)
    print("1.Add task  2.Start task  3.Complete task  4.Delete task  5.View old completed tasks  6.Quit")
    
    choice = int(input("Choose an option: "))
    
    if choice == 1:
        add_task()
    elif choice == 2:
        start_task()
    elif choice == 3:
        complete_task()
    elif choice == 4:
        delete_task()
    elif choice == 5:
        view_archived_completed_tasks()
    elif choice == 6:
        exit()
    else:
        print("Enter valid choice")


def add_task():
    x = input("Enter task: ")
    y = input("Enter timeframe to complete task: ")
    with conn:
        c.execute("INSERT INTO tasks VALUES (:task, :time, :status, :task_no)", {"task": x, "time": y, "status": 0, "task_no": 1})
    task_list_ui()


def start_task():
    x = input("Enter task to start: ")
    with conn:
        c.execute("UPDATE tasks SET status = 1 WHERE task = (?)", (x,))
    task_list_ui()


def complete_task():
    x = input("Enter task to complete: ")
    with conn:
        c.execute("UPDATE tasks SET status = 2 WHERE task = (?)", (x,))
        c.execute("INSERT INTO completed_tasks SELECT * FROM tasks WHERE task = (?)", (x,))
    task_list_ui()


def delete_task():
    x = input("Enter task to delete: ")
    with conn:
        c.execute("DELETE FROM tasks WHERE task = (?)", (x,))
    task_list_ui()


def view_archived_completed_tasks():
    tasks = []
    for i in (c.execute("SELECT * FROM completed_tasks")):
        tasks.append(i)
    
    table = Table()
    table.add_column("Task")
    table.add_column("Time to complete")
    table.add_column("Status")
    table.add_column("#")

    for i in tasks:
        if i[2] == 2:
            emoji = "✅"

        table.add_row((str(i[0])), (str(i[1])), emoji, str(i[3]))

    print(table)
    print("^Completed tasks archive:^")

    task_list_ui()


create_table_tasks()
create_table_completed_tasks()
task_list_ui()
